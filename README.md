#Incio

[TOC]

##Descripción General

HHTV Studio es un software de mezclado de videos y audio, que permitie emitir contenidos hacia internet utilizando un servidor de streaming (Flash Media Server). El software utilizara varias fuentes de audio y video, las cuales se podrán manipular en vivo. Estas fuentes son:

- Webcams
- Archivos
- Micrófonos
- Aplicaciones de escritorio
- Smartphones con SO Android (a través de la aplicación [Ip Webcam](https://play.google.com/store/apps/details?id=com.pas.webcam))

A las fuentes de video se le podrán aplicar efectos tales como:

- Mover
- Posicionar en pantalla 
- Cambiar de tamaño
- Mostrar y ocultar. 

Por otra parte, las fuentes de audio podrán ser mezcladas dentro de una misma salida y modificar sus propiedades de volumen.

##Licencia

HHTV Studio ha sido liberado bajo licencia [LGPLv3](https://www.gnu.org/licenses/lgpl.html)